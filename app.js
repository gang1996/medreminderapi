require("dotenv").config();
const jwt = require("jsonwebtoken");

const express = require("express");
const db = require("./utils/database");
const router = require("./routes/routes");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use((_, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

//Sync db
db.sequelize.sync();

app.use(router);

module.exports = app;
