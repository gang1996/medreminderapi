const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("medreminder3", "root", "root", {
  dialect: "mysql",
  host: "localhost",
});

//test database connection
sequelize
  .authenticate()
  .then(() => console.log("Database connect"))
  .catch((err) => console.log(err));

const db = {};
db.sequelize = sequelize;

//Models
db.users = require("../models/user")(sequelize, Sequelize);
db.contacts = require("../models/contact")(sequelize, Sequelize);
db.appointments = require("../models/appointment")(sequelize, Sequelize);
db.medications = require("../models/medication")(sequelize, Sequelize);

//Relations
db.users.hasMany(db.contacts, {
  foreignKey: "user_id",
});

db.contacts.belongsTo(db.users, {
  foreignKey: "user_id",
});

db.contacts.hasMany(db.appointments, {
  foreignKey: "contact_id",
});

db.appointments.belongsTo(db.contacts, {
  foreignKey: "contact_id",
});

db.users.hasMany(db.appointments, {
  foreignKey: "user_id",
});

db.appointments.belongsTo(db.users, {
  foreignKey: "user_id",
});

db.users.hasMany(db.medications, {
  foreignKey: "user_id",
});

db.medications.belongsTo(db.users, {
  foreignKey: "user_id",
});

module.exports = db;
