module.exports = (sequelize, Sequelize) => {
  const Appointment = sequelize.define(
    "appointments",
    {
      appointment_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      contact_id: {
        type: Sequelize.INTEGER,
      },
      contact_name: {
        type: Sequelize.STRING,
      },
      date: {
        type: Sequelize.DATE,
      },
      note: {
        type: Sequelize.STRING,
      },
      place: {
        type: Sequelize.STRING,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      event_id: {
        type: Sequelize.INTEGER,
      },
    },
    {
      timestamps: false,
    }
  );
  return Appointment;
};
