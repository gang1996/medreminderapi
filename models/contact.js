module.exports = (sequelize, Sequelize) => {
  const Contact = sequelize.define(
    "contacts",
    {
      contact_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      hospital: {
        type: Sequelize.STRING,
      },
      specialties: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
    },
    {
      timestamps: false,
    }
  );

  return Contact;
};
