module.exports = (sequelize, Sequelize) => {
  const Medication = sequelize.define(
    "medications",
    {
      medication_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      medication_name: {
        type: Sequelize.STRING,
      },
      medication_desc: {
        type: Sequelize.STRING,
      },
      as_needed: {
        type: Sequelize.BOOLEAN,
      },
      start_date: {
        type: Sequelize.DATE,
      },
      until_date: {
        type: Sequelize.DATE,
      },
      end_date: {
        type: Sequelize.DATE,
      },
      radio_value: {
        type: Sequelize.STRING,
      },
      recurrence_type: {
        type: Sequelize.INTEGER,
      },
      date_list: {
        type: Sequelize.JSON,
      },
      show_time: {
        type: Sequelize.JSON,
      },
      select_time: {
        type: Sequelize.DATE,
      },
      every_x_days_index: {
        type: Sequelize.INTEGER,
      },
      calendar_event_id: {
        type: Sequelize.JSON,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
    },
    {
      timestamps: false,
    }
  );
  return Medication;
};
