const database = require("../utils/database");
const Contact = database.contacts;

//Create Contact
const createContact = (req, res) => {
  const { name, specialties, hospital, phone, email } = req.body;
  const user_id = req.user.user_id;
  Contact.create({
    user_id,
    name,
    specialties,
    hospital,
    phone,
    email,
  })
    .then(() => {
      res.status(200).json({ message: "Contact created" });
    })
    .catch((err) => {
      res.status(502).json({ message: "Error while creating the contact" });
    });
};

//Edit Contact
const editContact = (req, res) => {
  const { name, specialties, hospital, phone, email, contact_id } =
    req.body;
  const updateData = {
    name,
    specialties,
    hospital,
    phone,
    email,
  };

  Contact.update(updateData, {
    where: {
      contact_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "edited contact successfully" })
    )
    .catch(() =>
      res.status(400).json({ message: "error while edited contact" })
    );
};

//Delete Contact
const deleteContact = (req, res) => {
  const { contact_id } = req.body;

  Contact.destroy({
    where: {
      contact_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "deleted contact successfully" })
    )
    .catch(() =>
      res.status(400).json({ message: "error while deleted contact " })
    );
};

//Get All Contact By user_id
const getContactByUserId = (req, res) => {
  const user_id = req.user.user_id;
  console.log(user_id);
  Contact.findAll({
    where: {
      user_id,
    },
  })
    .then((dbContact) =>
      res.status(200).json({ message: "here is your contact", data: dbContact })
    )
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: "error while getting contact" });
    });
};


module.exports = {
  createContact,
  deleteContact,
  editContact,
  getContactByUserId,
};
