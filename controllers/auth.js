require("dotenv").config();

const md5 = require("md5");
const jwt = require("jsonwebtoken");

const database = require("../utils/database");
const User = database.users;

//Register
const Register = (req, res) => {
  const { email, password } = req.body;
  //check if email exists
  User.findOne({
    where: {
      email,
    },
  })
    .then((dbUser) => {
      if (dbUser) {
        return res.status(409).json({ message: "Email already exists" });
      } else if (email && password) {
        return User.create({
          email: email,
          password: md5(password),
        })
          .then(() => {
            res.status(200).json({ message: "User created" });
          })
          .catch((err) => {
            console.log(err);
            res.status(502).json({ message: "Error while creating the user" });
          });
      }
    })
    .catch((err) => console.log("error", err));
};

//Log in
const Login = (req, res) => {
  const { email, password } = req.body;
  //check if email exists
  User.findOne({
    where: {
      email,
    },
  })
    .then((dbUser) => {
      if (!dbUser) {
        res.status(404).json({ message: "User not found" });
      } else {
        if (md5(password) === dbUser.password) {
          const accessToken = jwt.sign(
            { email, user_id: dbUser.user_id },
            process.env.ACCESS_TOKEN_SECRET
          );
          res.status(200).json({
            message: "log in success",
            accessToken: accessToken,
            user_id: dbUser.user_id,
          });
        } else {
          res.status(401).json({ message: "Invalid credentials" });
        }
      }
    })
    .catch((err) => {
      console.log("error", err);
    });
};

module.exports = {
  Login,
  Register,
};
