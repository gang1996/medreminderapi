const database = require("../utils/database");
const Appointment = database.appointments;

//Add Appointment
const addAppointment = (req, res) => {
  const { contact_id, contact_name, date, note, place, event_id } = req.body;
  const user_id = req.user.user_id;
  Appointment.create({
    contact_id,
    contact_name,
    date,
    note,
    place,
    user_id,
    event_id,
  })
    .then(() => res.status(200).json({ message: "Appointment created" }))
    .catch((err) => {
      console.log(err);
      res.status(502).json({ message: "Error while creating the appointment" });
    });
};

//Edit Appointment
const editAppointment = (req, res) => {
  const { contact_name, date, note, place, appointment_id, event_id } =
    req.body;
  const updateAppointmentData = {
    contact_name,
    date,
    note,
    place,
    event_id,
  };

  Appointment.update(updateAppointmentData, {
    where: {
      appointment_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "edited appointment successfully" })
    )
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: "error while edited appointment" });
    });
};

//Delete Appointment
const deleteAppointment = (req, res) => {
  const { appointment_id } = req.body;

  Appointment.destroy({
    where: {
      appointment_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "deleted appointment successfully" })
    )
    .catch(() =>
      res.status(400).json({ message: "error while deleted appointment" })
    );
};

//Get All Contact By user_id
const getAppointmentByUserId = (req, res) => {
  const user_id = req.user.user_id;
  Appointment.findAll({
    where: {
      user_id,
    },
    order: [["date", "ASC"]],
  })
    .then((dbAppointment) =>
      res
        .status(200)
        .json({ message: "here is your appointment", data: dbAppointment })
    )
    .catch((err) => {
      res.status(400).json({ message: "error while getting appointment" });
    });
};

//Get All Appointment by contact_id
const getAppointmentByContactId = (req, res) => {
  const contact_id = req.body.contact_id;
  Appointment.findAll({
    where: {
      contact_id,
    },
  })
    .then((dbAppointment) =>
      res
        .status(200)
        .json({ message: "This is Doctor's appointment", data: dbAppointment })
    )
    .catch((err) => {
      res.status(400).json({ message: "error while getting appointment" });
    });
};

module.exports = {
  addAppointment,
  editAppointment,
  deleteAppointment,
  getAppointmentByUserId,
  getAppointmentByContactId,
};
