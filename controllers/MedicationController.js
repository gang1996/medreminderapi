const { Op } = require("sequelize");
const database = require("../utils/database");
const Medication = database.medications;
const moment = require("moment");

//Create Medication
const createMedication = (req, res) => {
  const {
    medication_name,
    medication_desc,
    as_needed,
    start_date,
    until_date,
    end_date,
    radio_value,
    recurrence_type,
    date_list,
    show_time,
    select_time,
    every_x_days_index,
    calendar_event_id,
  } = req.body;
  const user_id = req.user.user_id;
  Medication.create({
    medication_name,
    medication_desc,
    as_needed,
    start_date,
    until_date,
    end_date,
    radio_value,
    recurrence_type,
    date_list,
    show_time,
    select_time,
    every_x_days_index,
    calendar_event_id,
    user_id,
  })
    .then((resData) => {
      res.status(200).json({ message: "Medication created", data: resData });
    })
    .catch((err) => {
      res.status(502).json({ message: "Error while creating the Medication" });
    });
};

//Edit Medication
const editMedication = (req, res) => {
  const {
    medication_name,
    medication_desc,
    as_needed,
    start_date,
    until_date,
    end_date,
    radio_value,
    recurrence_type,
    date_list,
    show_time,
    select_time,
    every_x_days_index,
    medication_id,
    calendar_event_id,
  } = req.body;

  const updateMedicationData = {
    medication_name,
    medication_desc,
    as_needed,
    start_date,
    until_date,
    end_date,
    radio_value,
    recurrence_type,
    date_list,
    show_time,
    select_time,
    every_x_days_index,
    calendar_event_id,
  };

  Medication.update(updateMedicationData, {
    where: {
      medication_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "edited medication successfully" })
    )
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: "error while edited medication" });
    });
};

//Delete Appointment
const deleteMedication = (req, res) => {
  const { medication_id } = req.body;

  Medication.destroy({
    where: {
      medication_id,
    },
  })
    .then(() =>
      res.status(200).json({ message: "deleted medication successfully" })
    )
    .catch(() =>
      res.status(400).json({ message: "error while deleted medication" })
    );
};

//Get All Medication By User
const getMedicationByUserId = (req, res) => {
  const user_id = req.user.user_id;
  Medication.findAll({
    where: {
      user_id,
    },
  })
    .then((dbMedication) =>
      res
        .status(200)
        .json({ message: "here is your medication", data: dbMedication })
    )
    .catch((err) => {
      res.status(400).json({ message: "error while getting medication" });
    });
};

//Get Medication By Date
const getMedicationByDate = (req, res) => {
  const todayDate = moment().set({ hours: 0, minutes: 0, seconds: 0 });
  const user_id = req.user.user_id;
  Medication.findAll({
    where: {
      user_id,
      as_needed: false,
      [Op.or]: [
        {
          [Op.and]: {
            start_date: {
              [Op.lte]: todayDate,
            },
            end_date: {
              [Op.gte]: todayDate,
            },
          },
        },
        {
          [Op.and]: {
            start_date: {
              [Op.lte]: todayDate,
            },
            end_date: {
              [Op.is]: null,
            },
          },
        },
      ],
    },
  })
    .then((dbMedication) =>
      res.status(200).json({
        message: "here is your medication",
        data: dbMedication,
        date: todayDate.format("DD/MM/YYYY"),
      })
    )
    .catch((err) => {
      res.status(400).json({ message: "error while getting medication" });
    });
};

module.exports = {
  createMedication,
  editMedication,
  deleteMedication,
  getMedicationByUserId,
  getMedicationByDate,
};
