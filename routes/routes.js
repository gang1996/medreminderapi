const express = require("express");
const router = express.Router();
const AuthController = require("../controllers/auth");
const ContactController = require("../controllers/ContactController");
const AppointmentController = require("../controllers/AppointmentController");
const MedicationController = require("../controllers/MedicationController");
const auth = require("../middleware/auth-middleware");

//Auth Api
router.post("/auth/login", AuthController.Login);

router.post("/auth/register", AuthController.Register);

//Contact Api
router.post("/contact/create", auth, ContactController.createContact);

router.put("/contact/edit", ContactController.editContact);

router.delete("/contact/delete", ContactController.deleteContact);

router.get("/contact/getbyuserid", auth, ContactController.getContactByUserId);

//Appointment Api
router.post("/appointment/create", auth, AppointmentController.addAppointment);

router.put("/appointment/edit", AppointmentController.editAppointment);

router.delete("/appointment/delete", AppointmentController.deleteAppointment);

router.get(
  "/appointment/getbyuserid",
  auth,
  AppointmentController.getAppointmentByUserId
);

router.get(
  "/appointment/getbycontactid",
  AppointmentController.getAppointmentByContactId
);

//Medication Api
router.post("/medication/create", auth, MedicationController.createMedication);

router.put("/medication/edit", MedicationController.editMedication);

router.delete("/medication/delete", MedicationController.deleteMedication);

router.get(
  "/medication/getbyuserid",
  auth,
  MedicationController.getMedicationByUserId
);

router.get(
  "/medication/getbydate",
  auth,
  MedicationController.getMedicationByDate
);

module.exports = router;
